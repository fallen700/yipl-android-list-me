import 'package:get/state_manager.dart';
import 'package:list_me/Model/posts.dart';
import 'package:list_me/Model/user.dart';
import 'package:list_me/services/remote_services.dart';


class PostController extends GetxController{
  List<Post> _postList = <Post>[].obs;
  List<Post> get postlist => _postList;

  @override


  void fetchPost(int userId)  async {
    var posts = await RemoteServices.fetchPost(userId);
    if(posts != null){
      _postList = posts;
    }


  }


}