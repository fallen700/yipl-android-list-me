import 'package:get/state_manager.dart';
import 'package:list_me/Model/user.dart';
import 'package:list_me/services/remote_services.dart';



class UserController extends GetxController{
  var userList = List<User>().obs;

  @override
  void onInit() {
    fetchUsers();
    super.onInit();
  }

  void fetchUsers()  async {
    var users = await RemoteServices.fetchUser();
    if(users != null){
      userList.value = users;
    }
  }
}
