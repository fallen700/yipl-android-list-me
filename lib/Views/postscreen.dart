import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:list_me/Model/posts.dart';
import 'package:list_me/Views/commentscreen.dart';
import 'package:list_me/services/remote_services.dart';


class UserPosts extends StatelessWidget {
  final int userId;
  final String userName;
  const UserPosts(this.userId,this.userName);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        elevation: 15,
        backgroundColor: Colors.blueGrey,
        title: Text("Posts of :" + userName),
      ),
      body: FutureBuilder <List<Post>>(
          future: RemoteServices.fetchPost(userId),
          builder: (context, snapshot) {
            if(snapshot.hasData){
              final  posts = snapshot.data;

              return ListView.separated(
                itemCount: posts.length,

                itemBuilder: (context, index){
                  final post = posts[index];
                  return Card(
                    color: Colors.red.shade50,
                    borderOnForeground: true,
                    margin: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                    shadowColor: Colors.black,
                    elevation: 20,
                    child: Column(
                      children: [
                        Text("Post ID: " +post.id.toString(),
                            style: TextStyle(fontSize: 18)),


                        SizedBox(height: 15),
                        Text("Title: "+ post.title,
                            style: TextStyle(fontSize: 18)),
                        Text("Body: "+post.body,
                            style: TextStyle(fontSize: 18)),

                        MaterialButton(onPressed: (){
                          Get.to(UserComment(post.id));


                        },
                          color: Colors.cyanAccent,
                          elevation: 15,
                          visualDensity: VisualDensity.adaptivePlatformDensity,
                          child: Text("Comments"),
                        ),



                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index){
                  return SizedBox(
                    height: 15,
                  );
                },
              );
            }
            return Center(child: CircularProgressIndicator());

          }
      ),
    );
  }
}
