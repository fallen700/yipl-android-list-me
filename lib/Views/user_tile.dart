import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:list_me/Model/user.dart';
import 'package:list_me/Views/postscreen.dart';


class UserTile extends StatelessWidget {

  final User user;
  UserTile(this.user);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Get.to(UserPosts(user.id,user.name));


      },
      child: Card(
        color: Colors.red.shade50,
        borderOnForeground: true,
        margin: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
        shadowColor: Colors.black,

        elevation: 20,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("ID: " +user.id.toString(),
                style: TextStyle(fontSize: 25,
                    color: Colors.grey.shade900),),
              Text("Name: "+ user.name,
                style: TextStyle(fontSize: 25,
                    color: Colors.black),),
              Text("Email: " +user.email,
                style: TextStyle(fontSize: 25),),
              MaterialButton(onPressed: (){
                Get.to(UserPosts(user.id,user.name));

              },
                  elevation: 50,
                  color: Colors.cyanAccent,
                  visualDensity: VisualDensity.adaptivePlatformDensity,

                  child: Text("POSTS",
                    style: TextStyle(fontSize: 20),))


            ],
          ),
        ),
      ),
    );
  }
}
