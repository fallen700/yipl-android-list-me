import 'package:flutter/material.dart';
import 'package:list_me/Model/comments.dart';
import 'package:list_me/services/remote_services.dart';


class UserComment extends StatelessWidget {
  final int postID;

  UserComment(this.postID);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text("Comments on post of PostID:" + "$postID"),
      ),
      backgroundColor: Colors.cyanAccent,
      body: FutureBuilder <List<Comment>>(
          future: RemoteServices.fetchComment(postID),
          builder: (context, snapshot) {
            if(snapshot.hasData){
              final  comments = snapshot.data;

              return ListView.separated(
                itemCount: comments.length,
                itemBuilder: (context, index){
                  final comment = comments[index];
                  return Card(
                    elevation: 25,
                    color: Colors.red.shade50,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [

                        Text("User Name: "+comment.name,
                            style: TextStyle(fontSize: 18)),
                        SizedBox(height: 15),
                        Text("Email: "+ comment.email,
                            style: TextStyle(fontSize: 18)),
                        Text("Comment: "+comment.body,
                            style: TextStyle(fontSize: 18)),
                        SizedBox(height: 20,)

                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index){
                  return SizedBox(height: 12,);
                },
              );
            }
            return Center(child: CircularProgressIndicator());

          }
      ),
    );
  }
}






