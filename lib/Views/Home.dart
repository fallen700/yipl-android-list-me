import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:list_me/Controller/usercontroller.dart';

import 'package:list_me/Views/user_tile.dart';

class Home extends StatelessWidget {
  final UserController userController = Get.put(UserController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,

        ),
        body: Container(
          color: Colors.blue.shade100,
          child: Column(

            children: [
              Padding(padding: const EdgeInsets.all(16),
                child: Row(
                  children: [
                    Expanded(child: Text(
                      'Hello , here is your list',
                      style: TextStyle(fontSize: 25,color: Colors.deepPurple),
                    ),

                    ),
                  ],
                ),
              ),
              Expanded(
                  child: Obx(() => StaggeredGridView.countBuilder(
                      crossAxisCount: 1,
                      itemCount: userController.userList.length,
                      crossAxisSpacing: 16,
                      mainAxisSpacing: 16,

                      itemBuilder: (context, index){
                        return UserTile(userController.userList[index]);
                      },
                      staggeredTileBuilder: (index) =>StaggeredTile.fit(1)),
                  )),

            ],),
        )
    );
  }
}
