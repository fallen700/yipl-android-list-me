import 'package:http/http.dart' as http;
import 'package:list_me/Model/comments.dart';
import 'package:list_me/Model/posts.dart';
import 'package:list_me/Model/user.dart';



import 'package:list_me/Views/user_tile.dart';

class RemoteServices {
  static var client = http.Client();
  final User user;


  RemoteServices(this.user);


  static Future <List<User>> fetchUser() async {
    var response = await client.get(
        'https://jsonplaceholder.typicode.com/users/');
    if (response.statusCode == 200) {
      var jsonString = response.body;
      return userFromJson(jsonString);
    }
    else {
      //error message
      return null;
    }
  }
  static Future <List<Post>> fetchPost(int userID) async{


    var resp = await client.get('https://jsonplaceholder.typicode.com/users/$userID/posts');
    if(resp.statusCode == 200){
      var jsonPosts = resp.body;
      return postFromJson(jsonPosts);
    }
    else{
      return null;
    }
  }
  static Future <List<Comment>> fetchComment(int postID) async{

    var resp = await client.get('https://jsonplaceholder.typicode.com/posts/$postID/comments');
    if(resp.statusCode == 200){
      var jsonComments = resp.body;
      return commentFromJson(jsonComments);
    }
    else{
      return null;
    }
  }



}